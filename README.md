The scripts in `packer-virtualbox-builder` build an AMI that can be used to 
build [Packer](http://www.packer.io) images for the Virtualbox provider.

The AMI itself is built using Packer, runs Ubuntu 14.04, 
and comes installed with:
* Oracle Java 8
* Virtualbox

The AMI is deployed to the `us-east-1` region. Change `ubuntu-trusty.json` if 
you wish to use another region.

> Note: The AMI has been designed to be used as a Jenkins slave using the 
[Jenkins Amazon EC2 Plugin](https://wiki.jenkins-ci.org/display/JENKINS/Amazon+EC2+Plugin).

# Project Structure

~~~~~~~~~~~
- cookbooks/vbox_cookbook - Chef recipes to install Virtualbox, Java, Git, and Packer before baking the AMI
- ubuntu-trusty.json - The packer configuration file.
~~~~~~~~~~~

# Build Instructions

## Pre-requisites

* Packer is installed and on your `$PATH`
* Berkshelf is installed and on your `$PATH`

### OS X

#### First: Install Ruby using RVM [Optional]

* Install RVM
~~~~~~~~~~~~~
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
\curl -sSL https://get.rvm.io | bash -s stable
~~~~~~~~~~~~~

* Ensure these lines are in your `bash_profile` or `bashrc`
~~~~~~~~~~~~~
[[ -s "$HOME/.rvm/scripts/rvm" ]] && . "$HOME/.rvm/scripts/rvm"
PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
~~~~~~~~~~~~~

* Source your rc or profile
~~~~~~~~~~~~~
. ~/.bash_profile
~~~~~~~~~~~~~

* Install Ruby
~~~~~~~~~~~~~
rvm install 2.1.2
~~~~~~~~~~~~~

* Use the newly installed version of Ruby
~~~~~~~~~~~~~
rvm use 2.1.2
~~~~~~~~~~~~~

#### Installing Packer

* Install Homebrew
~~~~~~~~~~~
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
~~~~~~~~~~~

* Install [Brew Cask](http://caskroom.io/), and then Packer using Brew Cask.
~~~~~~~~~~~
brew install caskroom/cask/brew-cask #Install Brew Cask
brew cask install packer #Install Packer
~~~~~~~~~~~

* Ensure `packer` is on your `PATH`
~~~~~~~~~~~~~
which packer
~~~~~~~~~~~~~

#### Installing Berkshelf

* Install Berkshelf
~~~~~~~~~~~~~
gem install berkshelf
~~~~~~~~~~~~~

* Ensure `berks` is on your `PATH`
~~~~~~~~~~~~~
which berks
~~~~~~~~~~~~~

## Build AMI

* Set the `ACCESS_KEY` and `SECRET_KEY` environment variables to your AWS access
key and secret access key.
* Run 
~~~~~~~~~~~
  ./build_packer_image
~~~~~~~~~~~

The `build_packer_image` script:
* Downloads all of the cookbooks required by Chef to set up the software on the
AMI and puts them into the `vendor/cookbooks` directory.
* Runs `packer build` to create the AMI

## Run Chef Cookbook tests

The Chef cookbook contains a bunch of integration tests written using 
[Kitchen](http://www.kitchen.ci). To run these tests, do:

~~~~~~~~~~~
cd cookbooks/vbox_cookbook
bundle install
kitchen test
~~~~~~~~~~~
