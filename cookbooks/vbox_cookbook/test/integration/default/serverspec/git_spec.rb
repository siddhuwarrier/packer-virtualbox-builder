require 'serverspec'

set :backend, :exec

describe 'Git' do
  it "should be installed" do
    expect(package('git')).to be_installed
  end
end
