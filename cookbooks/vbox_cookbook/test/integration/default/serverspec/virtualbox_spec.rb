require 'serverspec'

set :backend, :exec

describe 'Virtualbox' do
  it "should be installed" do
    expect(package('virtualbox-4.3')).to be_installed
  end
  it "should be running the vboxadd and vboxadd-service" do
    expect(service('vboxadd')).to be_running
    expect(service('vboxadd')).to be_enabled
    expect(service('vboxadd-service')).to be_running
    expect(service('vboxadd-service')).to be_enabled
  end
end
