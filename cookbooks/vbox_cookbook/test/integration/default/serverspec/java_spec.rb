require 'serverspec'

set :backend, :exec

describe command('java -version') do
  its(:stdout) { should contain('1.8') }
  its(:exit_status) { should eq 0 }
end
