require 'serverspec'

set :backend, :exec

describe command('packer version') do
  its(:exit_status) { should eq 0 }
end
