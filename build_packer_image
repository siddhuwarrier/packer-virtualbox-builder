#!/bin/bash
set -xe


function validate() {
   PACKER_CMD=`which packer` 
   if [[ -z $PACKER_CMD ]]; then
      help
      exit 1
   fi
   if [[ -z $ACCESS_KEY ]]; then
      help
      exit 1
   fi
   if [[ -z $SECRET_KEY ]]; then
      help
      exit 1
   fi
   if [[ -z $3 ]]; then
      PACKER_JSON=`pwd`/ubuntu-trusty.json
   else
      PACKER_JSON=$3
   fi
}

function help() {
    echo "Usage: ACCESS_KEY=[aws-access-key-id] SECRET_KEY=[aws-secret-access-key] \
       build_packer_image [path/to/packer/json]"
    echo "This script also expects the `packer` and `berkshelf` commands to be \
       in \$PATH. Refer to README.md for instructions on installing packer and \
       berkshelf"
    exit 1
}

function installBerks() {
   local cur_dir=$(pwd)
   find $cur_dir -name Berksfile| while read file; do
      cd `dirname $file`
      berks vendor $cur_dir/vendor/cookbooks
   done
}

function cleanUp() {
   rm -fr `pwd`/vendor
}

function buildPackerImg() { 
   $PACKER_CMD build -var aws_access_key=$ACCESS_KEY -var aws_secret_key=$SECRET_KEY $PACKER_JSON
}

function main() {
   validate
   cleanUp
   installBerks
   buildPackerImg
}

main
